# Digitaliser les écoles de manière souveraine


### Table of contents
1. [L'éducation et le monde du numérique](#educode-intro)
  1. [À la frontière de l'éducation](#educode-temps)
  1. [L'équipe : Des profs avant tout](#educode-apprentissage)
  1. [Un opérateur européen basé sur l'open source](#educode-rgpd)
  1. [Des données stockées dans les universités](#educode-souverainete)

## Educode, c'est quoi ? <a name="educode-intro"></a>

Educode est une entreprise qui veut accompagner le monde de l'éducation face aux défis du numérique.

Avec de nombreux partenaires, Educode met en place le **consortium Educode**.

### Mission et philosophie

La mission d'Educode (asbl, srl, consortium) est d'améliorer l'enseignement belge francophone en apportant les outils, formations, soutiens et accompagnements nécessaires, aux professeurs, aux élèves et à leurs parents, aux administrateurs d'écoles et aux autres acteurs de l'enseignement obligatoire.

La philosophie d'Educode est centrée autour du partage, de la liberté, et de la souveraineté de chacun. 

C'est pourquoi :

* les solutions proposées sont uniquement des logiciels libres,
* les utilisateurs pourront toujours récupérer leurs données sans difficultés et ne seront jamais pris en otage (la portabilité des données et l'interopérabilité seront garanties.)
* les données sont stockées en Belgique sur des serveurs gérés par les universités et autres partenaires belges et pas exploitées ni vendues,
* l'ensemble du travail de création d'Educode sera publié sous licence libre (Creative Commons CC-BY-SA, GNU GPL, ...)

Information sur le choix :
* <https://gsara.tv/teletravailler/logiciels-libres-erg/>
* <https://www.domainepublic.net/L-ERG-le-choix-du-libre.html>

### À la frontière de l'éducation <a name="educode-temps"></a>

EduCode est née en 2017 et a fait ses premiers pas en 2018 en organisant la première grande conférence francophone belge sur le numérique dans l'éducation. La conférence a été un succès et a fait des émules pour être réorganisée à Lille et donner lieu à d'autres conférences en Belgique.

Les conférences continuent et reprennent en intensité pour répondre aux besoins criants des professeurs pendant la pandémie de COVID-19 et c'est sur leur demande, que se met en place l'idée de EduCode Consortium.

L'objectif du consortium EduCode c'est :
1. Fournir les outils numériques, dont les écoles ont besoin, clé sur porte.
  
  Ce qui inclut des outils:
  * Pédagogiques
  * Administratifs
  * de Communication
  * de Gestion de communauté
2. Fournir les formations pour ces outils
3. Fournir du support pour ces outils
4. Accompagner les communautés des utilisateurs, tant responsables administratifs que enseignants.
**5. Animer la communauté des profs autour des outils numériques et de l'innovation dans l'enseignement
6. Organiser des plans de formations, des activités culturelles comme congrès, conférences, cours, expositions, ...
**
### L'équipe : Des profs avant tout <a name="educode-apprentissage"></a>
L'équipe d'Educode, c'est des profs avant tout et en même temps, des professionnels du numérique.

#### 1. Nicolas Pettiaux

Nicolas est professeur depuis qu'il est sorti des études. Déjà à l'école, il était engagé dans les Jeunesses Scientifiques, engagé dans l'éducation aux sciences puis au sortir des études comme assistant en maths, ensuite en secondaire dans les écoles du réseau WBE, d'abord à Woluwe-Saint-Lambert puis à Braine-l'Alleud. Il rejoint ensuite le privé pour 1 an en travaillant pour l' "International School of Brussels" (ISB) puis continuer en enseignant dans le supérieur, tant en haute école (La Cambre, ESI) qu'à l'ULB. Enfin, après avoir travaillé dans le modèle de pédagogie active de l'École Active, il revient comme professeur de math et physique dans le réseau des écoles libres.
En parallèle, il a toujours poussé le numérique et les logiciels libres. Dès sa thèse, il s'investit dans l'IT et l'installation d'outils numériques pour ses classes puis au ministère de la Région Bruxelloise, puis développe une salle informatique pour l'école de ses enfants. Il est également fondateur ou membre actif de nombreuses associations autour des logiciels libres et a organisé de nombreuses conférences à ce sujet.
Nicolas a aussi été IT manager du ministère de la Région de Bruxelles Capitale et de la Cocof. Il a une bonne expérience du privé car il a travaillé plusieurs années chez BNPParibasFortis comme process manager et consultant interne. 

Ce profil combiné de prof, d'expert IT et du logiciel libre le place à l'intersection des activités d'EduCode.

#### 2. Erick Mascart

Erick n'est pas prof. Enfin, c'est ce qu'on pourrait croire. Toute sa carrière, il a travaillé dans l'informatique, mais toujours dans l'informatique pour l'éducation. Dès 2001, où il travaillait déjà comme IT Manager pour le ministre de l'éducation jusqu'à aujourd'hui, où il combine ses deux passions, l'informatique et l'enseignement en mettant en place des outils ceux qu'EduCode propose et forme les professeurs à les utiliser.

En fait, Erick est prof, il connaît leurs préoccupations et mieux encore, il résoud les problèmes des profs pour mettre en place le numérique dans leurs écoles depuis 10 ans.

#### 3. Jason Pettiaux

L'enseignement coule dans les veines de la famille Pettiaux, avec deux grand-pères, prof de français et de mécanique des fluides, une tante prof de math et un père prof de physique, math et informatique. Jason est né dans l'enseignement quand il était petit.

Lui-même prof particulier de math, physique et chimie, il est aussi né dans le monde numérique. Ingénieur diplômé en 2017, le numérique c'est sa génération qui le connaît le mieux. Lancé dans le médical, c'est l'entrepreneuriat qui l'intéresse.
Né dans le logiciel libre, devant batailler avec Linux et Wine pour installer ses jeux, le logiciel libre c'est la seule option.

C'est tout naturellement qu'il rejoint le projet Educode quand Nicolas lui propose !


### Un opérateur européen basé sur le logiciel libre <a name="educode-rgpd"></a>

EduCode travaille exclusivement avec des logiciels libres parce que au coeur de ses valeurs, on trouve le partage de la connaissance et c'est toute la philosophie du logiciel libre.
<!-- 
Un logiciel libre, c'est un logiciel dont:
**L'utilisation, l'étude, la modification et la duplication par autrui en vue de sa diffusion sont permises, techniquement et légalement**
<https://fr.wikipedia.org/wiki/Logiciel_libre>
-->

Travailler uniquement avec du logiciel libre a plusieurs avantages. 

D'abord, tout le monde peut vérifier quelles types de données sont collectées ou transmises, en effet le code est lisible.
Ensuite, il y a une grande communauté qui utilise ces logiciels donc il y a peu de bugs car ceux-ci sont rapidement découverts par la communauté.
Enfin, étant basé sur le principe du partage, chaque logiciel libre peut être librement adapté pour répondre au mieux à la demande de son utilisateur, puisque son code source est accessible.

L'objectif d'EduCode est de profiter de l'opportunité offertes par les logiciels libres existants spécialisés dans le monde de l'enseignement et par son engagement et son travail, les améliorer encore.

### Des données stockées dans les universités <a name="educode-souverainete"></a>

Le logiciel est sûr, ou en tout cas tout le monde y a accès et peut vérifier ce qui s'y fait, reste la question critique du stockage des données.

EduCode vise à contribuer au développement des outils de l'enseignement mais le risque d'abus sur les données est toujours présent. Pour le mitiger, il est indispensable que les écoles aient la possibilité de quitter le système et emporter leurs données où elles le veulent, et il est aussi indispensable que ce soit une institution publique qui héberge ses données pour garantir la souveraineté des écoles sur celles-ci.

Pour combiner ces deux demandes exigeantes, l'équipe EduCode a négocié avec les centres de calcul des universités pour que ce soient eux qui hébergent les serveurs et donc les données. De plus, cette association renforce les liens entre écoles et universités améliorant d'autant notre système scolaire.

Ce n'est pas tout. Les outils que Educode souhaite mettre en place sont déjà utilisés dans les universités et hautes écoles, c'est donc un pas de plus vers l'harmonisation des standards pour faciliter la transition des élèves des études secondaires au supérieur.






