# Transformer les écoles de manière souveraine à l'heure du numérique.


### Table of contents
1. [L'éducation et le monde du numérique](#intro)
  1. [L'ère du temps](#temps)
  1. [L'apprentissage et l'éducation](#apprentissage)
  1. [Les données et le RGPD](#rgpd)
  1. [La souveraineté de l'état et les écoles](#souverainete)

## L'éducation et le monde du numérique <a name="intro"></a>

### L'ère du temps <a name="temps"></a>
L'arrivée du numérique a bousculé les codes dans la société entière et a rendu possible quantité de choses impossibles jusque là. De grandes universités américaines ont créé de nouveaux modèles d'enseignement à l'aide du numérique, par exemple, l'enseignement distribué avec la FabAcademy du MIT ou l'enseignement en ligne automatisé avec CS50 de Harvard, et leurs cours ont un succès phénoménal mais sont aussi extrêmement efficaces.

Le COVID-19 a lui aussi bousculé la société et forcé tout le monde à travailler en ligne indépendamment de la perméabilité de chacun au changement. Tout le monde est d'accord pour dire que le numérique est nécessaire aujourd'hui, autant les profs que les directeurs d'écoles, sans parler des élèves. Il est temps qu'on réponde à cette ambition parce que aujourd'hui, en particulier, pendant cette crise du COVID, les profs font grise mine. Si des outils sont parfois à leur disposition, ils sont souvent inappropriés. Soit ils n'ont pas été formés à leurs usages, soit ces outils dépendent des compagnies américaines aujourd'hui illégales, soit carrément inexistants !


### L'apprentissage et l'éducation <a name="apprentissage"></a>
L'éducation n'est pas soumis aux mêmes lois que l'entreprise et s'improviser professeur mène souvent à des échecs cuisants.
On ne le répètera jamais assez, chaque élève est différent et nécessite que le professeur s'adapte à lui. Pour ce faire, il est indispensable que le professeur reste libre dans son enseignement.

On l'a dit, l'éducation est un marché mûr à l'innovation. Pour cette raison, de nombreux éditeurs de logiciels de gestion d'entreprise (ERP) ont voulu passer le pas et répondre au besoin de l'éducation avec succès parfois. Par contre, si les solutions de gestion d'école sont satisfaisantes chez les directeurs, quand ces mêmes éditeurs se sont lancés dans la gestion de l'apprentissage, les Learning Management Systems (LMS), les profs ont perdu cette liberté et s'en plaignent amèrement, les témoignages que nous avons reçu à ce sujet sont nombreux.

Prenons un exemple issu d'un de ces témoignages: le journal de classe. Dans le LMS mis en place dans cette athénée, les professeurs doivent eux-mêmes encoder les devoirs et doivent le faire avant la fin de la journée sans quoi le journal de classe est bloqué jusqu'au prochain cours.

Cela pose différents problèmes mais le plus important est celui de la responsabilité et de l'apprentissage de celle-ci par l'élève. En effet, de nombreux élèves se plaignent de ne pas arriver à s'organiser or la première chose pour apprendre est de la faire soi-même. Si le prof écrit lui-même dans le journal de classe, l'élève n'apprend pas à s'organiser d'une part mais d'autre part, il rejette la responsabilité de son manque d'organisation sur le système en général, et le prof en particulier.

Comment en vouloir à l'éditeur du logiciel, il ne connaît pas le monde de l'enseignement et ses besoins, son domaine c'est le software et le logiciel d'entreprise.

Heureusement, les profs eux-mêmes résolvent leurs propres problèmes et développent quantité de logiciels répondant à leurs besoins. Ceux-ci vont du LMS créé par des profs au logiciel de tableau noir partagé, en passant par celui de téléconférence spécialisé en enseignement.


### Les données et le RGPD <a name="rgpd"></a>
La digitalisation est au coeur des préoccupations actuelles et pas que pour des questions de performances ou des questions sanitaires. Les préoccupations stratégiques et politiques actuelles des deux côtés de l'Atlantique et en Europe particulièrement sont très orientées sur la souveraineté de l'état face aux géants du Web, les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).
Ces préoccupations grandissant depuis plus de 10 ans ont aboutis à la création du RGPD, adopté en 2016 et bientôt au DSA (Digital Service Act) et DMA (Digital Market Act) en cours de négociation au parlement européen.
<!---
your comment goes here
and here
-->
Concernant la collecte et l'utilisation des données, le RGPD a défini un cadre très clair et est en application depuis plus de 2 ans. En résumé, les éditeurs de software ne peuvent pas récolter de données sans le consentement éclairé des utilisateurs sur leur utilisation future.

Ce point est très important car, en juillet 2020, les mécanismes de transfert de données vers les États-Unis, ont été jugés illégaux par la Cour de Justice de l'Union Européenne dans l'arrêt Schrems 2. Sont en cause plusieurs programmes de surveillance américains fondés sur l’article 702 du Foreign Intelligence Surveillance Act (FISA) de 1978 et sur l’Executive Order (E.O.) 12333 de 1981, qui permettent à des agences de renseignement de collecter et traiter massivement des données, y compris relatives à des résidents européens.(<https://www.village-justice.com/articles/arret-schrems-chute-privacy-shield-les-responsables-traitement-doivent-repenser,36284.html>)

Cette décision doit donc amener les responsables de traitement à privilégier le recours à dessous-traitants situés au sein de l’Union Européenne ou dans des pays disposant d’une décision d’adéquation.

A défaut, ils devront s’assurer que le pays n’est pas doté de lois permettant à des agences étatiques d’accéder à des données de résidents européens.

De là, les GAFAM sont de facto illégaux et hors jeu étant donné qu'ils sont tous américains et qu'ils ont une dépendance profonde à la collecte et l'exploitation des données.

### La souveraineté de l'état et les écoles <a name="souverainete"></a>
L'éducation et les écoles sont au coeur de notre démocratie et la vendre ou permettre à des entreprises privées de s'approprier le cerveau de nos enfants serait très dangereux. À ce titre, confier les données de nos enfants à des entreprises privées quand bien même elles sont européennes fait courir le risque d'abus.


### Conclusions et problématiques-clés

Sur base de ces différents points, il apparaît qu'il est nécessaire, aujourd'hui plus que jamais d'amener les bénéfices du numérique dans le monde de l'éducation en respectant la liberté et les besoins des professeurs. Ceci devant être fait dans le respect des lois et en maintenant la souveraineté de notre éducation.
