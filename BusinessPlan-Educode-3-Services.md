# Digitaliser les écoles de manière souveraine


### Table of contents
1. [Comment ça marche ?](#)
  1. [En pratique](#)
      1. [Mise en place](#)
      1. [Au jour le jour](#)
  1. [Pour les profs](#)
  1. [Pour les directeurs](#)
  1. [Pour les élèves et leurs parents](#)
1. [Les services](#)
  1. [Logiciels](#)
      1. [Pédagogie](#)
          1. [Chamilo](#)
          1. [Moodle](#)      
      1. [Administration scolaire](#)
          1. [Générale](#)
              1. [ZeusEdu](#)
              1. [RosarioSIS](#)
              1. [OpenSIS](#)
          1. [Conception d'horaires](#)
          1. [Comptabilité: Noalyss](#)
      1. [Travail collaboratif](#)
          1. [Téléconférence](#)
              1. [BigBlueButton](#)
              1. [Jitsi](#)
          1. [Stockage en ligne: NextCloud](#)
          1. [Mail](#)
          1. [Réseau social scolaire: edutwit](#)
  1. [Formations](#)
      1. [Comment ça marche ?](#)
      1. [Nos formateurs](#)
      1. [Les profs référents](#)
      1. [La communauté](#)
      1. [](#)
  
  1. [Communauté](#)
      1. [Discourse](#)
      1. [Locale](#)
      1. [Nationale](#)
      1. [](#)
      
  
## Comment ça marche ? <a name=""></a>

<!--
 1. LMS (learning management system) : pour que les membres de l'école structurent tous les documents pédagogiques et les travaux
    1. Moodle → <https://fr.wikipedia.org/wiki/Moodle>
    2. Chamilo → <https://fr.wikipedia.org/wiki/Chamilo>
2. Vidéo conférence
    1. jitsi → <https://fr.wikipedia.org/wiki/Jitsi>
    2. BigBlueButton → <https://fr.wikipedia.org/wiki/BigBlueButton>
3. Messagerie
    1. Thunderbird
    2. Mailcow
    3. sogo ou zimbra ou zoho ou cozycloud ou nextcloud
4. Travail collaboratif
    1. nextcloud → <https://fr.wikipedia.org/wiki/Nextcloud>
    2. etherpad
    3. ethercalc
    4. Agora-Project → <https://www.agora-project.net/>
5. Système d'information scolaire (SIS)
    1. RosarioSIS → <https://www.rosariosis.org/>
    2. OpenSIS → <https://opensis.com/>
    3. ZeusEdu → <https://www.sio2.be/wiki/doku.php> et <https://github.com/ymairesse/ZeusEdu>
    4. HappySchool → <https://linuxfr.org/news/happyschool-logiciel-de-gestion-scolaire>
6. Comptabilité
    1. noalyss → <https://www.noalyss.eu/>
7. Conception d'horaires (cf EDT)
8. Gestion des paiements dans l'école (notamment rfid, cf itschool)
9. Réseau social scolaire
    1. edutwit → [https://fr.vikidia.org/wiki/Édutwit](https://fr.vikidia.org/wiki/%C3%89dutwit) et  <https://www.edutwit.fr/>

-->