# Digitaliser les écoles de manière souveraine
<!--
 Commentaires
-->

### Table des matières
1. [Développement en phase](#DeveloppementPhase)
  1. [Phase 1 : Prototype : 01/01/2021 -> 15/01/2021 -> 30/01/2021](#)
  1. [Phase 2 : Déploiement et Test des formations : lancés dans 5 écoles : 15/01/2021 -> 30/03/2021](#)
  1. [Phase 3 : Préparation Scale up : 01/01/2021 -> 30/05/2021](#)
  1. [Phase 4 : Lancement at Scale : 01/06/2021 -> 30/09/2021](#)
  1. [Planning 2021](#)
  1. [](#)
  1. [](#)
  

## Développement en phase <a name="DeveloppementPhase"></a>
### 1. Phase 1 : Prototype : 01/01/2021 -> 15/01/2021 -> 30/01/2021

** Success metrics **

* Software scalable : prêt à être déployé, tous les softs sont prêt et dockerisé
* Set up et migration facile :
  
    1. Temps : Il est possible de déployé dans une école en peu de temps
    2. Documentation : le processus pour déployé dans une école existe et la liste des info nécessaires d'une école est disponible
  
* Les outils suivants sont concernés:
    * Chamilo
    * Moodle
    * ZeusEdu
    * Mail
    * NextCloud
    * Single SAML ou Authenthik
* Hébergé en local chez All2All

Tout ceci doit être fait pour le 15 janvier 2021.
Pour le 30 janvier, nous validons la performance en lançant dans une école pilote.

Pour valider cette performance, il faudra lancer la communauté dans un Discourse ou RocketChat 

### 2. Phase 2 : Déploiement et Test des formations : lancés dans 5 écoles : 15/01/2021 -> 30/03/2021

** Success metrics ** 

* Création et set up d'une instance par école
* Formation de trois profs responsables/ écoles
* Payement d'un fee (réduit ?)
* Plan du contrôle qualité : Monitoring de la charge, discussion avec les profs

### 3. Phase 3 : Préparation Scale up : 01/01/2021 -> 30/05/2021
** Success metrics ** 
* Préparer autant d'écoles que possibles
* Faire des formations introductives
* Créer les instances démo
* Former des profs responsables dans les écoles ( > 30)
* Animer la communauté
* Préparer le dossier de presse pour l'annonce de juin
* Mettre en place l'équipe nécessaire pour le lancement de septembre

* Vendre pour 2021-2022

### 3. Phase 4 : Lancement at Scale : 01/06/2021 -> 30/09/2021
** Success metrics **
* Faire des formations introductives
* Organiser le plan de formation des > 1000 profs inscrits
* Créer les instances
* Former les profs responsables dans les écoles
* Former le personnel admin
* Préparer le dossier de presse pour l'annonce de septembre

## Planning 2021
* 15 janvier : Test fonctionnel: le pilote fonctionne sur la machine educode
* 30 janvier : Test de terrain : le pilote est installé dans une école
* 28 février :
* 15 mai  : la ministre Caroline Désir peut communiquer aux écoles
* 1 septembre 2021 : déploiement commercial dans plusieurs écoles de la CFWB (PO: WBE, Segec, felsi ...)
* 30 septembre 2021 : Formations continues

* 15 janvier 2022 : Conférence Educode - Best Practices 1
* 15 février 2022 : Conférence Educode - Best Practices 2
